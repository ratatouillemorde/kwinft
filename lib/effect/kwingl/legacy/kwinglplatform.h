/*
SPDX-FileCopyrightText: 2010 Fredrik Höglund <fredrik@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
#pragma once

// This header is deprecated, but installed for backwards compatibility. Do not include it anymore.
// Instead directly include the header below.
#include <kwingl/platform.h>
